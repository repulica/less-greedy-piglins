package repulica.lessgreedypiglins.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.mob.PiglinBrain;
import net.minecraft.entity.mob.PiglinEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;

@Mixin(PiglinBrain.class)
public abstract class MixinPiglinBrain {
	@Shadow protected static boolean isGoldenItem(Item item) {
		throw new UnsupportedOperationException("Mixin not applied!");
	}

	@Inject(method = "loot", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/mob/PiglinEntity;sendPickup(Lnet/minecraft/entity/Entity;I)V"), cancellable = true)
	private static void fixLoot(PiglinEntity piglin, ItemEntity item, CallbackInfo info) {
		if ((isGoldenItem(item.getStack().getItem()) || item.getStack().getItem() == Items.GOLD_NUGGET) && (item.getThrower() == null && item.getOwner() == null)) info.cancel();
	}
}
